package com.springsecurity.demo.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloSecurityController {

    @GetMapping(value = "getAdmin")
    public String getAdmin() {
        return "hello admin";
    }

    @PreAuthorize("hasAnyRole('普通用户')")
    @GetMapping(value = "getUser")
    public String getUser() {
        return "hello normal";
    }
}
