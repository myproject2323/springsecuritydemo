package com.springsecurity.demo.mapper;

import com.springsecurity.demo.entity.UserEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

public interface UserMapper {

    @Select({"<script>SELECT\n" +
            "\tt1.user_id as userId, \n" +
            "\tt1.username as username, \n" +
            "\tt1.`password` as password,\n" +
            "\tt3.`code` as  roleCode,\n" +
            "\tt3.name as roleName\n" +
            "FROM\n" +
            "\tsys_user t1\n" +
            "\tleft join sys_users_roles t2 on t1.user_id = t2.user_id\n" +
            "\tleft join sys_role t3 on t2.role_id = t3.role_id\n" +
            "\twhere username = #{username}</script>"})
    UserEntity findByUsername(String username);

}
