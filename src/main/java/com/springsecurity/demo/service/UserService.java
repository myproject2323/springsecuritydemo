package com.springsecurity.demo.service;

import com.springsecurity.demo.entity.UserEntity;

public interface UserService {

    public UserEntity findByUsername(String username);
}
