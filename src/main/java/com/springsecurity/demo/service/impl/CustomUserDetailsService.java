package com.springsecurity.demo.service.impl;

import com.springsecurity.demo.entity.UserEntity;
import com.springsecurity.demo.service.UserService;
import com.springsecurity.demo.util.DefaultPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: UserDetailsService
 * @Description: UserDetailsService
 * @Author oyc
 * @Date 2020/12/29 10:21
 * @Version 1.0
 */
@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    UserService userService;
    @Autowired
    DefaultPasswordEncoder defaultPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userService.findByUsername(username);
        if (userEntity == null) {
            throw new UsernameNotFoundException("not found");
        }
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_"+userEntity.getRoleName()));
//        User userDetails = new User(userEntity.getUsername(), defaultPasswordEncoder.encode(userEntity.getPassword()), authorities);
        User userDetails = new User(userEntity.getUsername(), userEntity.getPassword(), authorities);
        return userDetails;
    }
}
