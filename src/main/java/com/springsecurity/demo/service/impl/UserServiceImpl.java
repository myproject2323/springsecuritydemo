package com.springsecurity.demo.service.impl;

import com.springsecurity.demo.dao.UserDao;
import com.springsecurity.demo.entity.UserEntity;
import com.springsecurity.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserDao userDao;

    @Override
    public UserEntity findByUsername(String username) {
        return userDao.findByUsername(username);
    }
}
