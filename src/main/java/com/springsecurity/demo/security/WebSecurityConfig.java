package com.springsecurity.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

//@Configuration
//@EnableWebSecurity // 启用 spring Security
//@EnableGlobalMethodSecurity(prePostEnabled = true) // 会拦截注解了 @PreAuthorize 注解的配置
/**
 * 1.想要开启 spring 方法级安全，需要在已经添加了 @Configuration 注解的类上再添加
 * @EnableGlobalMethodSecurity 注解
 * 2.如何配置方法级别的权限控制
 * 使用注解 @PreAuthorize("hasAnyRole('admin')") 即可指定访问级别的角色
 */

public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        /**
//         * 基于内存的方式，构建两个用户账号
//         * 需要指定密码加密方式，不然会报错误：There is no PasswordEncoder mapped for the id "null"
//         */
//        auth.inMemoryAuthentication().
//                withUser("weipeng").
//                password(passwordEncoder().
//                        encode("123456")).roles("admin");
//        auth.inMemoryAuthentication().
//                withUser("lll").
//                password(passwordEncoder().
//                        encode("123")).roles("normal");
//
//    }

    /**
     * 通过 @Bean 注入指定 PasswordEncoder
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
