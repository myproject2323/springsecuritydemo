package com.springsecurity.demo.config;

import com.springsecurity.demo.entity.UserEntity;
import com.springsecurity.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

//@Component
public class CustomUserDetailService{
//    @Autowired
//    UserService userService;
//    @Autowired
//    PasswordEncoder passwordEncoder;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        /**
//         * 1. 通过 username 获取到 userInfo 信息
//         * 2. 通过 User(UserDetails) 返回 UserDetails
//         */
//        UserEntity userEntity = userService.findByUsername(username);
//        if (userEntity == null) {
//            throw new UsernameNotFoundException("not found");
//        }
//        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//        authorities.add(new SimpleGrantedAuthority("ROLE_"+userEntity.getRoleName()));
//        User userDetails = new User(userEntity.getUsername(), passwordEncoder.encode(userEntity.getPassword()), authorities);
//        return userDetails;
//    }
}
