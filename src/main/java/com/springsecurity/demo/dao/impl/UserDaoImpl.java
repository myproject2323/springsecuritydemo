package com.springsecurity.demo.dao.impl;

import com.springsecurity.demo.dao.UserDao;
import com.springsecurity.demo.entity.UserEntity;
import com.springsecurity.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    UserMapper userMapper;

    @Override
    public UserEntity findByUsername(String username) {
        return userMapper.findByUsername(username);
    }
}
