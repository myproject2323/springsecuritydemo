package com.springsecurity.demo.dao;

import com.springsecurity.demo.entity.UserEntity;

public interface UserDao {

    public UserEntity findByUsername(String username);
}
