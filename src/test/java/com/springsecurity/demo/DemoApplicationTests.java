package com.springsecurity.demo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.Base64Codec;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@SpringBootTest
class DemoApplicationTests {

    @Test
    void testCreatToken() {
        // 创建一个 JwtBuilder 对象
        JwtBuilder jwtBuilder = Jwts.builder()
                // 声明的标识{"jti":"888"}
                .setId("888")
                // 主体，用户{"sub":"Rose"}
                .setSubject("Rose")
                .setIssuedAt(new Date())
                // 签名手段，参数1：算法，参数2：盐
                .signWith(SignatureAlgorithm.HS256, "yjxxt");
        // 获取 jwt 的 token
        String token = jwtBuilder.compact();
        System.out.println(token);

        // 三部分的 Base64 解密
        System.out.println("_______________________");
        String[] split = token.split("\\.");
        System.out.println(Base64Codec.BASE64.decodeToString(split[0]));
        System.out.println(Base64Codec.BASE64.decodeToString(split[1]));
        // 无法解密
        System.out.println(Base64Codec.BASE64.decodeToString(split[2]));
    }

    @Test
    public void test02() {
        long time = System.currentTimeMillis();
        time = time + 60 * 1000;
        // 创建一个 JwtBuilder 对象
        JwtBuilder jwtBuilder = Jwts.builder()
                // 声明的标识{"jti":"888"}
                .setId("888")
                // 主体，用户{"sub":"Rose"}
                .setSubject("Rose")
                .setIssuedAt(new Date())
                // 签名手段，参数1：算法，参数2：盐
                .signWith(SignatureAlgorithm.HS256, "yjxxt")
                .claim("username", "admin")
                .claim("address", "地址")
                .setExpiration(new Date(time));
        // 获取 jwt 的 token
        String token = jwtBuilder.compact();
        System.out.println(token);

        // 三部分的 Base64 解密
        System.out.println("_______________________");
        String[] split = token.split("\\.");
        System.out.println(Base64Codec.BASE64.decodeToString(split[0]));
        System.out.println(Base64Codec.BASE64.decodeToString(split[1]));
        // 无法解密
        System.out.println(Base64Codec.BASE64.decodeToString(split[2]));
    }

    @Test
    public void test03() {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiJSb3NlIiwiaWF0IjoxNjMyMTE3Njc1LCJ1c2VybmFtZSI6ImFkbWluIiwiYWRkcmVzcyI6IuWcsOWdgCIsImV4cCI6MTYzMjExNzczNX0.UVbJ7y0NaIJvD-I401IPs0ERWu0hiCSpv7UmD6dwufg";
        Claims claims = Jwts.parser()
                .setSigningKey("yjxxt")
                .parseClaimsJws(token)
                .getBody();

        System.out.println("id:" + claims.getId());
        System.out.println("subject:" + claims.getSubject());
        System.out.println("issuedAt:" + claims.getIssuedAt());
        DateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("签发时间：" + sf.format(claims.getIssuedAt()));
        System.out.println("过期时间：" + sf.format(claims.getExpiration()));
        System.out.println("当前时间：" + sf.format(new Date()));
        System.out.println("自定义用户名：" + claims.get("username"));
        System.out.println("自定义地址：" + claims.get("address"));
    }

    public void recharge(String priceCode) {
        if ("充值一个月会员".equals(priceCode)) {
            // todo something
        } else if ("充值三个月会员".equals(priceCode)) {
            // todo something
        } else if ("充值一年会员".equals(priceCode)) {
            // todo something
        }
    }


//    public void payNotify(String priceCode) {
//        if ("充值一个月会员".equals(priceCode)) {
//            new OneMonthVipStrategy().recharge(priceCode);
//        } else if ("充值三个月会员".equals(priceCode)) {
//            new ThreeMonthVipStrategy().recharge(priceCode);
//        } else if ("充值一年会员".equals(priceCode)) {
//            new YearVipStrategy().recharge(priceCode);
//        }
//    }
}
